package com.mcd.common.model;

import java.io.Serializable;
import java.util.Date;

/**
 * @author heng.jia
 * @date 2018/7/18 上午1:17
 */
public class BaseObject implements Serializable{
    private static final long serialVersionUID = 151824335682999718L;

    //common fields
    private String createdUserUuid;
    private Date createdTime;
    private String updatedUserUuid;
    private Date updatedTime;
    //逻辑删除时使用
    private Boolean deleted = false;

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public String getCreatedUserUuid() {
        return createdUserUuid;
    }

    public void setCreatedUserUuid(String createdUserUuid) {
        this.createdUserUuid = createdUserUuid;
    }

    public Date getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(Date createdTime) {
        this.createdTime = createdTime;
    }

    public String getUpdatedUserUuid() {
        return updatedUserUuid;
    }

    public void setUpdatedUserUuid(String updatedUserUuid) {
        this.updatedUserUuid = updatedUserUuid;
    }

    public Date getUpdatedTime() {
        return updatedTime;
    }

    public void setUpdatedTime(Date updatedTime) {
        this.updatedTime = updatedTime;
    }

    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }
}

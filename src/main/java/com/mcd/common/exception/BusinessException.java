package com.mcd.common.exception;

/**
 * @author heng.jia
 * @date 2018/6/20 下午9:19
 */
public class BusinessException extends RuntimeException{

    protected String errorCode;

    public BusinessException(String msg, Throwable t) {
        super(msg, t);
    }

    public BusinessException(String errorCode,String msg) {
        super(msg);
        this.errorCode = errorCode;
    }


    public BusinessException(String msg) {
        super(msg);
    }


    public String getErrorCode() {
        return errorCode;
    }
}

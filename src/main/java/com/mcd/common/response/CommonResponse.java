package com.mcd.common.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.mcd.common.constant.CommonResponseCode;
import com.mcd.common.constant.CommonResponseType;
import com.mcd.common.util.McdStringUtil;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.util.Map;

/**
 * @author heng.jia
 * @date 2018/6/20 下午9:20
 */
@ApiModel
public class CommonResponse implements Serializable {

    private static final long serialVersionUID = 1838185517770622465L;

    @ApiModelProperty(value = "响应码,1表示成功", example="1")
    private String responseCode;
    @ApiModelProperty(value = "响应消息")
    private String responseMessage;

    @ApiModelProperty(value = "响应类型")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String responseType;//ERROR,WARN

    @ApiModelProperty(value = "自定义错误数据map",example = "${name}${age}${test}")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Map<String,Object> errorDataMap;//错误的map${name}${age}${test}出错，这个是格式 map 记录name,age,test的值；

    public Map<String, Object> getErrorDataMap() {
        return errorDataMap;
    }

    public void setErrorDataMap(Map<String, Object> errorDataMap) {
        this.errorDataMap = errorDataMap;
    }

    public String getResponseType() {
        if(McdStringUtil.isNotBlank(responseType) && !CommonResponseCode.RC_SUCCESS.getResponseCode().equals(responseCode)){
            responseType = CommonResponseType.ERROR.toString();
        }
        return responseType;
    }

    public void setResponseType(String responseType) {
        this.responseType = responseType;
    }

    public String getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }

    public CommonResponse() {
        super();
    }

    public CommonResponse(String responseCode, String responseMessage) {
        super();
        this.responseCode = responseCode;
        this.responseMessage = responseMessage;
    }

    public CommonResponse(CommonResponseCode commonResponseCode) {
        super();
        this.responseCode = commonResponseCode.getResponseCode();
        this.responseMessage = commonResponseCode.getResponseMessage();
    }


    public String getResponseMessage() {
        return responseMessage;
    }

    public void setResponseMessage(String responseMessage) {
        this.responseMessage = responseMessage;
    }
}

package com.mcd.common.util.http;

/**
 * @author heng.jia
 * @date 2018/6/20 下午9:29
 * 使用的时候每个调用最好创建自己的连接池
 *
 * 每个功能需要考虑多人在线的时候建立连接时间，经常发起请求的连接需要创建连接池
 *
 *
 * demo: 如果是http 线程池
 * class fxtApiHttpClient{
 *
 *     private HttpClientUtil fxtClient = HttpClientUitlFactory.getHttpPoolInstance(30,30)
 *
 * }
 *
 *
 */
public class HttpClientUitlFactory {

    /**
     * 获取单个连接，目前close是在方法内
     */
    public static HttpClientUtil getOneClientInstance(){
        return  new HttpClientUtil.Builder(1, 1).build();
    }


    /**
     * 如果http 连接池实例,默认这个就行了，默认10s的超时时间
     */
    public static HttpClientUtil getHttpPoolInstance(int maxTotal, int maxPerRoute){
        return  new HttpClientUtil.Builder(maxTotal, maxPerRoute).build();
    }


    /**
     * @param maxTotal
     * @param maxPerRoute
     * @param connectionRequestTimeout 指从连接池获取连接的timeout
     * @param connectTimeout 指客户端和服务器建立连接的timeout，
     * @param socketTimeout 指客户端从服务器读取数据的timeout
     */
    public static HttpClientUtil getHttpPoolInstance(int maxTotal, int maxPerRoute,int connectionRequestTimeout,int connectTimeout,int  socketTimeout){
        return  new HttpClientUtil.Builder(maxTotal, maxPerRoute)
                .setConnectionRequestTimeout(connectionRequestTimeout)
                .setConnectionTimeout(connectTimeout)
                .setSocketTimeout(socketTimeout)
                .build();
    }
}

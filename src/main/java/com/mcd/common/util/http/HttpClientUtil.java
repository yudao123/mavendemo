package com.mcd.common.util.http;

import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Map;

/**
 * @author heng.jia
 * @date 2018/6/20 下午9:29
 */
public class HttpClientUtil {
    private PoolingHttpClientConnectionManager cm;
    private static String EMPTY_STR = "";
    private static String UTF_8 = "UTF-8";
    private static Integer DEFUALT_TIMEOUT = 5000;
    private CloseableHttpClient client;
    public int maxTotal = 1;
    public int defaultMaxPerRoute = 1;
    public int connectionRequestTimeout = DEFUALT_TIMEOUT;
    public int socketTimeout = DEFUALT_TIMEOUT;
    public int connectTimeout = DEFUALT_TIMEOUT;

    private HttpClientUtil(Builder builder) {
        if (builder.getMaxTotalConnection() != null) {
            maxTotal = builder.getMaxTotalConnection();
        }

        if (builder.getOneHostMaxPerRoute() != null) {
            defaultMaxPerRoute = builder.getOneHostMaxPerRoute();
        }

        if (builder.getConnectionRequestTimeout() != null) {
            connectionRequestTimeout = builder.getConnectionRequestTimeout();
        }

        if (builder.getSocketTimeout() != null) {
            socketTimeout = builder.getSocketTimeout();
        }

        if (builder.getConnectionTimeout() != null) {
            connectTimeout = builder.getConnectionTimeout();
        }
        init();
    }

    private HttpClientUtil() {
    };

    private CloseableHttpClient init() {
        cm = new PoolingHttpClientConnectionManager();
        cm.setMaxTotal(maxTotal);// 整个连接池最大连接数
        cm.setDefaultMaxPerRoute(defaultMaxPerRoute);// 每路由最大连接数，默认值是2

        // !!! !!! make this configruable

        RequestConfig requestConfig = RequestConfig.custom().setConnectionRequestTimeout(connectionRequestTimeout)
                .setSocketTimeout(socketTimeout).setConnectTimeout(connectTimeout).build();

        client = HttpClientBuilder.create().setConnectionManager(cm).setDefaultRequestConfig(requestConfig).build();
        return client;
    }

    /**
     * 通过连接池获取HttpClient
     */
    public CloseableHttpClient getHttpClient() {
        return client;
    }

    public String get(String url) {
        HttpGet httpGet = new HttpGet(url);
        return getResult(httpGet);
    }

    public String get(String url, Map<String, Object> params) throws URISyntaxException {
        URIBuilder ub = new URIBuilder();
        ub.setPath(url);

        ArrayList<NameValuePair> pairs = covertParams2NVPS(params);
        ub.setParameters(pairs);

        HttpGet httpGet = new HttpGet(ub.build());
        return getResult(httpGet);
    }

    public String get(String url, Map<String, Object> headers, Map<String, Object> params)
            throws URISyntaxException {
        URIBuilder ub = new URIBuilder();
        ub.setPath(url);
        if(params!=null){
            ArrayList<NameValuePair> pairs = covertParams2NVPS(params);
            ub.setParameters(pairs);
        }


        HttpGet httpGet = new HttpGet(ub.build());
        for (Map.Entry<String, Object> param : headers.entrySet()) {
            httpGet.addHeader(param.getKey(), String.valueOf(param.getValue()));
        }
        return getResult(httpGet);
    }

    public String post(String url) {
        HttpPost httpPost = new HttpPost(url);
        return getResult(httpPost);
    }

    public String post(String url, Map<String, Object> params) throws UnsupportedEncodingException {
        HttpPost httpPost = new HttpPost(url);
        ArrayList<NameValuePair> pairs = covertParams2NVPS(params);
        httpPost.setEntity(new UrlEncodedFormEntity(pairs, UTF_8));
        return getResult(httpPost);
    }

    public String post(String url, Map<String, Object> headers, Map<String, Object> params)
            throws UnsupportedEncodingException {
        HttpPost httpPost = new HttpPost(url);

        for (Map.Entry<String, Object> param : headers.entrySet()) {
            httpPost.addHeader(param.getKey(), String.valueOf(param.getValue()));
        }

        ArrayList<NameValuePair> pairs = covertParams2NVPS(params);
        httpPost.setEntity(new UrlEncodedFormEntity(pairs, UTF_8));

        return getResult(httpPost);
    }

    /**
     * 发送一个 Post 请求, 使用指定的字符集编码.
     *
     * @param url
     * @param body
     *            RequestBody
     * @param mimeType
     *            例json
     * @param charset
     *            编码
     * @param connTimeout
     *            建立链接超时时间,毫秒.
     * @param readTimeout
     *            响应超时时间,毫秒.
     * @throws Exception
     */
    public String postWithJson(String url, String body, String mimeType) {
        HttpPost httpPost = new HttpPost(url);
        String result = "";
        if (StringUtils.isNotBlank(body)) {
            HttpEntity entity = new StringEntity(body, ContentType.create(mimeType, UTF_8));
            httpPost.setEntity(entity);
        }
        return getResult(httpPost);
    }

    public String postWithJson(String url, String body) {
        return postWithJson(url,body,"application/json");
    }

    public String postWithJson(String url, String body, String mimeType,Map<String, Object> headers) {
        HttpPost httpPost = new HttpPost(url);
        for (Map.Entry<String, Object> param : headers.entrySet()) {
            httpPost.addHeader(param.getKey(), String.valueOf(param.getValue()));
        }
        String result = "";
        if (StringUtils.isNotBlank(body)) {
            HttpEntity entity = new StringEntity(body, ContentType.create(mimeType, UTF_8));
            httpPost.setEntity(entity);
        }
        return getResult(httpPost);
    }

    public String postWithXml(String url, StringEntity se) throws UnsupportedEncodingException {
        HttpPost httpPost = new HttpPost(url);
        httpPost.setHeader("Content-Type", "application/xml");
        httpPost.setEntity(se);
        return getResult(httpPost);
    }

    private ArrayList<NameValuePair> covertParams2NVPS(Map<String, Object> params) {
        ArrayList<NameValuePair> pairs = new ArrayList<NameValuePair>();
        for (Map.Entry<String, Object> param : params.entrySet()) {
            pairs.add(new BasicNameValuePair(param.getKey(), String.valueOf(param.getValue())));
        }
        return pairs;
    }

    /**
     * 处理Http请求
     *
     * @param request
     * @return
     */
    private String getResult(HttpRequestBase request) {
        // CloseableHttpClient httpClient = HttpClients.createDefault();


        CloseableHttpClient httpClient = getHttpClient();
        HttpResponse res = null;
        try {
            CloseableHttpResponse response = httpClient.execute(request);
            // response.getStatusLine().getStatusCode();
            HttpEntity entity = response.getEntity();
            if (entity != null) {
                // long len = entity.getContentLength();// -1 表示长度未知
                String result = EntityUtils.toString(entity);
                response.close();
                // httpClient.close();
                return result;
            }
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            realaseIfNeed(httpClient);
        }
        return EMPTY_STR;
    }

    /**
     * 最大只有一个连接，就关闭了
     */
    private void realaseIfNeed(CloseableHttpClient httpclient) {
        if (this.maxTotal == 1) {
            try {
                httpclient.close();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
    }

    public static class Builder {
        private Integer maxTotalConnection;// 最大连接数
        private Integer oneHostMaxPerRoute;// 一个主机最大的连接

        // 连接池中取出连接的超时时间
        private Integer connectionRequestTimeout;

        // 网络与目标服务器建立连接的超时时间
        private Integer connectionTimeout;

        // 读取响应内容时间
        private Integer socketTimeout;

        public Integer getMaxTotalConnection() {
            return maxTotalConnection;
        }

        public void setMaxTotalConnection(Integer maxTotalConnection) {
            this.maxTotalConnection = maxTotalConnection;
        }

        public Integer getOneHostMaxPerRoute() {
            return oneHostMaxPerRoute;
        }

        public void setOneHostMaxPerRoute(Integer oneHostMaxPerRoute) {
            this.oneHostMaxPerRoute = oneHostMaxPerRoute;
        }

        public Integer getConnectionRequestTimeout() {
            return connectionRequestTimeout;
        }

        public Integer getConnectionTimeout() {
            return connectionTimeout;
        }

        public Integer getSocketTimeout() {
            return socketTimeout;
        }

        public Builder(Integer maxTotalConnection, Integer oneHostMaxPerRoute) {
            super();
            this.maxTotalConnection = maxTotalConnection;
            this.oneHostMaxPerRoute = oneHostMaxPerRoute;
        }

        public Builder setConnectionRequestTimeout(Integer connectionRequestTimeout) {
            this.connectionRequestTimeout = connectionRequestTimeout;
            return this;
        }

        public Builder setConnectionTimeout(Integer connectionTimeout) {
            this.connectionTimeout = connectionTimeout;
            return this;
        }

        public Builder setSocketTimeout(Integer socketTimeout) {
            this.socketTimeout = socketTimeout;
            return this;
        }

        public HttpClientUtil build() {
            return new HttpClientUtil(this);
        }
    }

}

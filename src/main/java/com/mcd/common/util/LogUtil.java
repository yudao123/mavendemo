package com.mcd.common.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.PrintWriter;
import java.io.StringWriter;

/**
 * @author heng.jia
 * @date 2018/7/18 上午12:50
 */
public class LogUtil {

    private final Logger log;

    private LogUtil(Class<?> clazz) {
        log =  LoggerFactory.getLogger(clazz);
    }

    private LogUtil() {
        log = LoggerFactory.getLogger(LogUtil.class);
    }

    public static LogUtil getLogger(Class<?> clazz) {
        return new LogUtil(clazz);
    }

    public boolean isDebugEnabled(){
        return false;
    }

    public  void info(String message){
        log.info(message);
    }

    public  void info(String message,Throwable t){
        log.info(message,t);
    }
    public  void info(String format,Object... arg ){
        log.info(format,arg);
    }

    public  void debug(String message){
        log.debug(message);
    }

    public void debug(String format, Object arg){
        log.debug(format,arg);
    }

    public void debug(String format, Object arg1, Object arg2){
        log.debug(format,arg1,arg2);
    }


    public  void error(String message){
        log.error(message);
    }

    public  void error(String message,Throwable t){
        log.error(message,t);
    }

    public void error(String format, Object... arguments){
        log.error(format,arguments);
    }

    public void logStackTrace(String exceptionFlag){
        StackTraceElement[] stackTraceElements = Thread.currentThread().getStackTrace();
        StringBuilder stringBuilder = new StringBuilder(exceptionFlag);
        for(StackTraceElement se : stackTraceElements){
            stringBuilder.append(se.toString());
            stringBuilder.append("\n");
        }
        error(stringBuilder.toString());
    }
    public static String processLogger(Throwable throwable){
        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw, true);
        throwable.printStackTrace(pw);
        pw.flush();
        sw.flush();
        return sw.toString();
    }

}

package com.mcd.common.util;

import com.alibaba.fastjson.JSON;

/**
 * @author heng.jia
 * @date 2018/6/21 下午7:56
 */
public class JsonUtil {
    public static String toJson(Object object){
        return JSON.toJSONString(object);
    }

    public static <T> T toObject(String json, Class<T> clazz) {
        return JSON.parseObject(json, clazz);
    }
}

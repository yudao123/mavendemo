package com.mcd.common.util;

import java.util.UUID;

/**
 * @author heng.jia
 * @date 2018/6/21 下午7:57
 */
public class UuidUtil {
    private UuidUtil(){

    }
    public static String geneUuidString(){
        return UUID.randomUUID().toString().replace("-", "");
    }
}

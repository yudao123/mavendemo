package com.mcd.common.util;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;

/**
 * @author heng.jia
 * @date 2018/6/20 下午9:42
 */
public class DecimalUtil {

    /**
     * 默认保留两位小数
     *
     * @param value
     * @return
     */
    public static double round(double value) {
        return round(value, 2);
    }

    /**
     * 默认保留6小数
     *
     * @param value
     * @return
     */
    public static double round6(double value) {
        return round(value, 6);
    }

    public static double round(double value, int i) {
        BigDecimal bd = new BigDecimal(String.valueOf(value));
        Number newValue = bd.setScale(i, RoundingMode.HALF_UP);
        return newValue.doubleValue();
    }

    //是否显示6位小数, false 是显示2位
    public static double financeRound(double vlaue ,boolean showSex){
        if(showSex){
            return round6(vlaue);
        }else{
            return round(vlaue);
        }
    }

    // 进行加法运算 ,double 相乘会出现精度问题
    public static double add(double d1, double d2)
    {
        BigDecimal b1 = new BigDecimal(String.valueOf(d1));
        BigDecimal b2 = new BigDecimal(String.valueOf(d2));
        return b1.add(b2).doubleValue();
    }
    public static double sub(double d1, double d2)
    {        // 进行减法运算
        BigDecimal b1 = new BigDecimal(String.valueOf(d1));
        BigDecimal b2 = new BigDecimal(String.valueOf(d2));
        return b1.subtract(b2).doubleValue();
    }
    public static double mul(double d1, double d2)
    {        // 进行乘法运算
        BigDecimal b1 = new BigDecimal(String.valueOf(d1));
        BigDecimal b2 = new BigDecimal(String.valueOf(d2));
        return b1.multiply(b2).doubleValue();
    }

    // 进行除法运算
    public static double div(double d1, double d2) {
        BigDecimal b1 = new BigDecimal(String.valueOf(d1));
        BigDecimal b2 = new BigDecimal(String.valueOf(d2));
        return b1.divide(b2,2).doubleValue();
    }

    public static double div(double d1, double d2,int scale) {
        BigDecimal b1 = new BigDecimal(String.valueOf(d1));
        BigDecimal b2 = new BigDecimal(String.valueOf(d2));
        return b1.divide(b2,scale,BigDecimal.ROUND_HALF_UP).doubleValue();
    }

    //格式化double 超过8位数，不采用科学计数显示，采用正确的数字显示
    public static String formatDoubleToTenShow (double value) {
        DecimalFormat df = new DecimalFormat("#.#");
        df.setMaximumFractionDigits(18); // 设置最大小数位
        return df.format(value);
    }

    public static double formatValue(Double value){
        return value==null?0d:value;
    }

    public static void main(String[] args) {

        System.out.println(round(345.2345234534, 4));
        System.out.println(round(4038.9999999999995,2));

    }

}

package com.mcd.common.util;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.FastDateFormat;
import org.springframework.context.i18n.LocaleContextHolder;

import java.text.ParseException;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.time.Clock;
import java.util.*;

/**
 * @author heng.jia
 * @date 2018/6/20 下午9:41
 * Date Utility Class used to convert Strings to Dates and Timestamps
 */
public final class DateUtil {

    //private static Log log = LogFactory.getLog(DateUtil.class);

    public static final String TIME_PATTERN = "HH:mm";

    private static final String TIME_PATTERN2 = "HH:mm:ss";

    public static final String YYYY_MM_DD_HH_MM_SS = "yyyy-MM-dd HH:mm:ss";
    public static final String YYYY_MM_DD_HH_MM_SS_SSS = "yyyy-MM-dd HH:mm:ss.SSS";

    public static final String YYYYMMDDHHMMSSSSS = "yyyyMMddHHmmssSSS";

    public static final String YYYY_MM_DD = "yyyy-MM-dd";

    public static final String YYYY_MM_DD_HH_MM = "yyyy-MM-dd HH:mm";

    public static final String YYYY_MM = "yyyy-MM";
    public static final String YYYY = "yyyy";

    /**
     * 格式 = yyyyMMddHHmmss
     */
    public static final String YYYYMMDDHHMMSS = "yyyyMMddHHmmss";
    public static final String ZEROTIME = " 00:00:00";
    public static final String FOURTIME = " 04:00:00";
    public static final String TOMORROW_ENDTIME = " 03:59:59";
    public static final String ENDTIME = " 23:59:59";
    public static final String DATE_NULL_STR = "1970-01-01 00:00:00";


    /**
     * Checkstyle rule: utility classes should not have public constructor
     */
    private DateUtil() {
    }

    public static String getLogTimestamp(){
        return formatDateTime(new Date(), YYYY_MM_DD_HH_MM_SS_SSS);
    }
    public static String formatDateTime(Date dateTime, String format) {
        if (dateTime == null) {
            return null;
        }
        return FastDateFormat.getInstance(format).format(dateTime);
    }

    /**
     * Return default datePattern (MM/dd/yyyy)
     *
     * @return a string representing the date pattern on the UI
     */
    public static String getDatePattern() {
        return YYYY_MM_DD;
    }

    public static String getDateTimePattern() {
        return DateUtil.getDatePattern() + " HH:mm:ss.S";
    }

    /**
     * This method attempts to convert an Oracle-formatted date in the form dd-MMM-yyyy to mm/dd/yyyy.
     *
     * @param aDate date from database as a string
     * @return formatted string for the ui
     */
    public static String getDate(Date aDate) {
        SimpleDateFormat df;
        String returnValue = "";

        if (aDate != null) {
            df = new SimpleDateFormat(getDatePattern());
            returnValue = df.format(aDate);
        }

        return (returnValue);
    }
    public static Date getDateByString(String pattern,String aDate) throws ParseException {
        SimpleDateFormat df;
        Date returnValue = null;

        if (aDate != null) {
            df = new SimpleDateFormat(pattern);
            returnValue = df.parse(aDate);
        }

        return (returnValue);
    }

    public static String formatDate(String date, String format) {
        if (date == null || "".equals(date)){
            return "";
        }
        Date dt = null;
        SimpleDateFormat inFmt = null;
        SimpleDateFormat outFmt = null;
        ParsePosition pos = new ParsePosition(0);
        date = date.replace("-", "").replace(":", "");
        if ((date == null) || ("".equals(date.trim())))
            return "";
        try {
            if (Long.parseLong(date) == 0L)
                return "";
        } catch (Exception nume) {
            return date;
        }
        try {
            switch (date.trim().length()) {
                case 14:
                    inFmt = new SimpleDateFormat("yyyyMMddHHmmss");
                    break;
                case 12:
                    inFmt = new SimpleDateFormat("yyyyMMddHHmm");
                    break;
                case 10:
                    inFmt = new SimpleDateFormat("yyyyMMddHH");
                    break;
                case 8:
                    inFmt = new SimpleDateFormat("yyyyMMdd");
                    break;
                case 6:
                    inFmt = new SimpleDateFormat("yyyyMM");
                    break;
                case 7:
                case 9:
                case 11:
                case 13:
                default:
                    return date;
            }
            if ((dt = inFmt.parse(date, pos)) == null)
                return date;
            if ((format == null) || ("".equals(format.trim()))) {
                outFmt = new SimpleDateFormat("yyyy年MM月dd日");
            } else {
                outFmt = new SimpleDateFormat(format);
            }
            return outFmt.format(dt);
        } catch (Exception ex) {
        }
        return date;
    }

    /**
     * This method generates a string representation of a date/time in the format you specify on input
     *
     * @param aMask   the date pattern the string is in
     * @param strDate a string representation of a date
     * @return a converted Date object
     * @throws java.text.ParseException when String doesn't match the expected format
     * @see java.text.SimpleDateFormat
     */
    public static Date convertStringToDate(String aMask, String strDate) throws ParseException {
        if (StringUtils.isEmpty(strDate)) {
            return null;
        }
        SimpleDateFormat df;
        Date date;
        df = new SimpleDateFormat(aMask);

        // if (log.isDebugEnabled()) {
        // log.debug("converting '" + strDate + "' to date with mask '" + aMask + "'");
        // }

        try {
            date = df.parse(strDate);
        } catch (ParseException pe) {
            // log.error("ParseException: " + pe);
            throw new ParseException(pe.getMessage(), pe.getErrorOffset());
        }

        return (date);
    }

    /**
     * This method returns the current date time in the format: MM/dd/yyyy HH:MM a
     *
     * @param theTime the current time
     * @return the current date/time
     */
    public static String getTimeNow(Date theTime) {
        return getDateTime(TIME_PATTERN, theTime);
    }

    /**
     * This method returns the current date in the format: MM/dd/yyyy
     *
     * @return the current date
     * @throws java.text.ParseException when String doesn't match the expected format
     */
    public static Calendar getToday() {

        Date today = new Date();
        SimpleDateFormat df = new SimpleDateFormat(getDatePattern());

        // This seems like quite a hack (date -> string -> date),
        // but it works ;-)
        String todayAsString = df.format(today);
        Calendar cal = new GregorianCalendar();
        try {
            cal.setTime(convertStringToDate(todayAsString));
        } catch (ParseException e) {

        }

        return cal;
    }

    /**
     * This method generates a string representation of a date's date/time in the format you specify on input
     *
     * @param aMask the date pattern the string is in
     * @param aDate a date object
     * @return a formatted string representation of the date
     * @see java.text.SimpleDateFormat
     */
    public static String getDateTime(String aMask, Date aDate) {
        String returnValue = "";

        if (aDate != null) {
            SimpleDateFormat df = new SimpleDateFormat(aMask);
            returnValue = df.format(aDate);
        }

        return returnValue;
    }

    /**
     * This method generates a string representation of a date based on the System Property 'dateFormat' in the format you specify on input
     *
     * @param aDate A date to convert
     * @return a string representation of the date
     */
    public static String convertDateToString(Date aDate) {
        return getDateTime(getDatePattern(), aDate);
    }

    /**
     * This method generates a string representation of a date based on the param 'pattern' you specify when you invoke this method
     *
     * @param aDate   A date to convert
     * @param aDate   A date to convert
     * @param pattern date pattern
     * @return a string representation of the date
     */
    public static String convertDateToString(Date aDate, String pattern) {
        return getDateTime(pattern, aDate);
    }

    /**
     * This method converts a String to a date using the datePattern
     *
     * @param strDate the date to convert (in format MM/dd/yyyy)
     * @return a date object
     * @throws java.text.ParseException when String doesn't match the expected format
     */
    public static Date convertStringToDate(final String strDate) throws ParseException {
        return convertStringToDate(getDatePattern(), strDate);
    }

    /**
     * 计算两个日期之间相差的天数
     *
     * @param smdate 较小的时间
     * @param bdate  较大的时间
     * @return 相差天数
     * @throws java.text.ParseException
     */
    public static int daysBetween(Date smdate, Date bdate) throws ParseException {
        return daysBetween(smdate,bdate,"yyyy-MM-dd");
    }


    /**
     * 计算两个日期相减
     * @param startDate 较小的时间
     * @param endDate  较大的时间
     * @return 相差
     */
    public static int timeBetween(Date startDate,Date endDate){
        try{
            SimpleDateFormat dateFormat=new SimpleDateFormat(YYYYMMDDHHMMSS);
            startDate=dateFormat.parse(dateFormat.format(startDate));
            endDate=dateFormat.parse(dateFormat.format(endDate));
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(startDate);
            Long start = calendar.getTimeInMillis();
            calendar.setTime(endDate);
            Long end = calendar.getTimeInMillis();
            if((end - start)<0){
                return -1;
            }else {
                return 1;
            }
        }catch (ParseException e){
            return  0;
        }
    }
    public static void main(String[] args) throws ParseException {
        Date start = DateUtil.convertStringToDate(DateUtil.YYYY_MM_DD_HH_MM,"2017-07-01 00:00:00");
        Date end = DateUtil.convertStringToDate(DateUtil.YYYY_MM_DD_HH_MM,"2017-05-27 14:21:00");
        timeBetween(start,end);
        DateUtil.convertDateToString(new Date(),"yyyyMMddHHmmssSSS");
//        long a = getTimeDifferenceMinute(start,end);
//        System.out.println(106d/3);
//        Calendar calendar = Calendar.getInstance();
//        calendar.setTimeInMillis(1494989791680L);
//        System.out.println(DateUtil.formatDateTime(calendar.getTime(),DateUtil.YYYY_MM_DD_HH_MM_SS));
//        //System.out.println(DateUtil.getFirstDateTimeOfMonth(new Date()));
    }

    /**
     * 计算两个日期之间相差的天数
     *
     * @param smdate 较小的时间
     * @param bdate  较大的时间
     * @param dateFormat 时间格式
     * @return 相差天数
     * @throws java.text.ParseException
     */
    public static int daysBetween(Date smdate, Date bdate, String dateFormat) throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);
        smdate = sdf.parse(sdf.format(smdate));
        bdate = sdf.parse(sdf.format(bdate));
        Calendar cal = Calendar.getInstance();
        cal.setTime(smdate);
        long time1 = cal.getTimeInMillis();
        cal.setTime(bdate);
        long time2 = cal.getTimeInMillis();
        long between_days = (time2 - time1) / (1000 * 3600 * 24);

        return Integer.parseInt(String.valueOf(between_days));
    }

    //获取设置为开始月份的第1天
    //如果设置的是27 号，那么开始日期就是上个月的28号-这个月的27号
    //如果设置的是31号，那么设置的就是当个月的1号

    /**
     *
     * @param year 页面上传递的实际年
     * @param month 页面上传递的实际月份
     * @param accountingDate
     * @return
     */
    public static Date getStartDate(int year, int month, int accountingDate) {
        Calendar c = Calendar.getInstance();
        c.set(year, month - 1, 1); // 设置为当月第一天
        c.add(Calendar.DATE, -1); // 向前推一天，也就是上个月最后一天，比如是2月28号或者29号
        if (accountingDate == -1 || accountingDate >= c.get(Calendar.DATE)) {
            // 如果财务截止日期没有设置，或者大于等于最后一天，这就是截止日期。
        } else {
            c.set(Calendar.DATE, accountingDate); // 否则设置为截止
        }
        c.set(Calendar.HOUR_OF_DAY, 0);
        c.set(Calendar.MINUTE, 0);
        c.set(Calendar.SECOND, 0);
        // 将截止日期往后推一天得到开始日期时间
        c.add(Calendar.DATE, 1);
        try {
            return new SimpleDateFormat("yyyy-MM-dd").parse(new SimpleDateFormat("yyyy-MM-dd").format(c.getTime()));
        } catch (ParseException e) {
            throw new RuntimeException("Should never happen.");
        }
    }

    //管理选项的日期，比上个月的最后一天，还大的话，就设置成最后一天，例如6月份是30，配置是31，那么取结束日期取30，如果配的是26，那么就取26
    // 如果小的话，设置成管理选项的对应的天数
    public static Date getEndDate(int year, int month, int accountingDate) {
        Calendar c = Calendar.getInstance();
        c.set(year, month, 1);
        c.add(Calendar.DATE, -1);
        //设置成上个月
        if (accountingDate == -1 || accountingDate >= c.get(Calendar.DATE)) {
            // ok
        } else {
            c.set(Calendar.DATE, accountingDate);
        }
        c.set(Calendar.HOUR_OF_DAY, 23);
        c.set(Calendar.MINUTE, 59);
        c.set(Calendar.SECOND, 59);
        c.set(Calendar.MILLISECOND, 999);
        return c.getTime();
    }

    /**
     * 取得日期的最后时间
     * @param date
     * @return
     */
    public static Date getEndTime(Date date) {
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        c.set(Calendar.HOUR_OF_DAY, 23);
        c.set(Calendar.MINUTE, 59);
        c.set(Calendar.SECOND, 59);
        //使用 999 查询 出 会 延后 一天  故  使用 998
        c.set(Calendar.MILLISECOND, 998);
        return c.getTime();
    }

    public static Date getStartDate(Date date, Integer accountingDate) {
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        return getStartDate(c.get(Calendar.YEAR), c.get(Calendar.MONTH) + 1, accountingDate);
    }

    public static Date getEndDate(Date date, Integer accountingDate) {
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        return getEndDate(c.get(Calendar.YEAR), c.get(Calendar.MONTH) + 1, accountingDate);
    }

    /**
     * 获取指定天数区间的天数集合
     *
     * @param startDate 开始日期
     * @param day       天数
     * @return 包括开始
     * @auther hui
     */
    public static List<String> getStartToEndDayList(Date startDate, Integer day) {
        List<String> resultList = new ArrayList<String>();
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(startDate);
        for (int i = 1; i <= day; i++) {
            resultList.add(convertDateToString(calendar.getTime()));
            calendar.add(Calendar.DATE, 1);
        }
        return resultList;
    }

    /**
     * 获取指定周数的的周几集合
     *
     * @param startDate 开始日期
     * @param weekNum   周数
     * @param week      周几
     * @return 包括开始
     * @auther hui
     * @time 2013-12-2
     */
    public static List<String> getStartToEndWeekListForWeekNum(Date startDate, Integer weekNum, Integer week) {
        List<String> resultList = new ArrayList<String>();
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(startDate);
        int day = calendar.get(Calendar.DAY_OF_WEEK);
        if (day == 1) {
            calendar.add(Calendar.DAY_OF_WEEK, -1);
        }
        if (week == 1) {
            calendar.add(Calendar.DAY_OF_WEEK, 7);
        }
        for (int i = 1; i <= weekNum; i++) {
            calendar.set(Calendar.DAY_OF_WEEK, week);
            resultList.add(convertDateToString(calendar.getTime()));
            calendar.add(Calendar.DATE, 7);
        }
        return resultList;
    }

    public static Integer getDayOfYear(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        return calendar.get(Calendar.DAY_OF_YEAR);
    }

    public static Integer getDay(Date date){
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        return calendar.get(Calendar.DAY_OF_MONTH);
    }

    public static Integer getMonth(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        return calendar.get(Calendar.MONTH) + 1;
    }

    public static Integer getYear(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        return calendar.get(Calendar.YEAR);
    }

    /**
     * 获取指定日期指定数的月份集合
     *
     * @param date     开始计算日期
     * @param monthNum 月数
     * @return
     */
    public static List<String> getFirstDayListForMonth(Date date, Integer monthNum) {
        List<String> resultList = new ArrayList<String>();
        GregorianCalendar calendar = (GregorianCalendar) Calendar.getInstance();
        calendar.setTime(date);
        for (int i = 1; i <= monthNum; i++) {
            calendar.set(Calendar.DAY_OF_MONTH, 1);
            resultList.add(convertDateToString(calendar.getTime()));
            calendar.add(Calendar.MONTH, 1);
        }
        return resultList;
    }

    /**
     * 获取指定日期后几天的日期
     *
     * @param date   指定日期
     * @param addDay 指定天数
     * @return
     */
    public static Date getAddDayNum(Date date, Integer addDay) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.DATE, addDay);
        return calendar.getTime();
    }
    /**
     * 获取指定日期后几月的日期
     *
     * @param date   指定日期
     * @param addMonth 指定月数
     * @return
     */
    public static Date getAddMonthNum(Date date, Integer addMonth) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.MONTH, addMonth);
        return calendar.getTime();
    }
    /**
     * 取某月最大天数
     *
     * @param date   指定日期
     * @return
     */
    public static Integer getMonthMaxDay(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        int lastDay = calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
        return lastDay;
    }

    /**
     * 获取日期前几天的日期
     *
     * @param d
     * @param day
     * @return
     */
    public static Date getDateBefore(Date d, int day) {
        Calendar now = Calendar.getInstance();
        now.setTime(d);
        now.set(Calendar.DATE, now.get(Calendar.DATE) - day);
        return now.getTime();
    }

    public static String getDateByDates(Date beginDate, int number) {
        Calendar beginCal = Calendar.getInstance();
        beginCal.setTime(beginDate);
        GregorianCalendar calendar = new GregorianCalendar(beginCal.get(Calendar.YEAR), beginCal.get(Calendar.MONTH), beginCal.get(Calendar.DATE));
        // calendar.add(GregorianCalendar.DATE, datas);
        calendar.add(Calendar.MONTH, number); // 得到前一个月
        String begin = new java.sql.Date(calendar.getTime().getTime()).toString();
        return begin;
    }

    /**
     * 获取时间差的分钟数
     *
     * @param start
     * @param end
     * @return
     */
    public static Long getTimeDifferenceMinute(Date start, Date end) {
        Long timeDiff = end.getTime() - start.getTime();
        return timeDiff / (60 * 1000);
    }

    public static Date addDay(Date date, int day) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.DAY_OF_MONTH, day);
        return cal.getTime();
    }

    public static Date clearTime(Date date) {
        try {
            return convertStringToDate(convertDateToString(date));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 根据业绩生效日期获取提成区间
     * 日期晚于财务截止日期，算下个月，管理选项是25 传递的是30，查询的是3月份数据，传递的是4实际代表5月份，getEndDate取的是上个月的日期，实际对应的日期是3.25
     * 传递的日期比配置的日期如果日期小于的话，例如管理选项配置的是31,传递的是30 例如是查询3月份 传递3实际代表的是4月getEndDate取的是上个月的日期，实际对应的日期是3.30
     */
    public static Date getCommissionDate(Date paymentCalcDate, Integer accountingDate) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(paymentCalcDate);
        //如果算的日期大于管理选项的日期，就算下一个月的日期
        if (cal.get(Calendar.DATE) > accountingDate) { // 如果日期晚于财务截止日期，算下一个月的。
            return getEndDate(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH) + 2 , accountingDate);
        } else {
            //如果日期小于的话，设置成
            return getEndDate(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH) + 1 , accountingDate);
        }
    }

    /**
     * 计算两个日期之间相隔的月份
     */
    public static int monthsBetween(Date startDate, Date endDate) {
        int months = 0;
        int startYear = getYear(startDate);
        int endYear = getYear(endDate);
        int startMonth = getMonth(startDate);
        int endMonth = getMonth(endDate);
        int startDay = getDayOfYear(startDate);
        int endDay = getDayOfYear(endDate);
        months = (endYear - startYear) * 12;
        months += endMonth - startMonth;
        if (endDay < startDay) {
            months = months - 1;
        }
        return months;
    }

    /**
     * 当前年的开始时间，即2012-01-01 00:00:00
     *
     * @return
     */
    public static Date getCurrentYearStartTime() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Calendar c = Calendar.getInstance();
        Date now = null;
        try {
            c.set(Calendar.MONTH, 0);
            c.set(Calendar.DATE, 1);
            now = sdf.parse(sdf.format(c.getTime()));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return now;
    }

    /**
     * 根据现在时间 获取 十二个月 之前的 时间
     * @return
     */
    public static Date getStartTime(){
        Calendar cal = Calendar.getInstance();//使用默认时区和语言环境获得一个日历。
        cal.add(Calendar.MONTH, -12);
        return cal.getTime();
    }

    /**
     * 当前年的结束时间，即2012-12-31 23:59:59
     *
     * @return
     */
    public static Date getCurrentYearEndTime() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Calendar c = Calendar.getInstance();
        Date now = null;
        try {
            c.set(Calendar.MONTH, 11);
            c.set(Calendar.DATE, 31);
            now = sdf.parse(sdf.format(c.getTime()));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return now;
    }

    /**
     * 获取月份的第一天
     *
     * @param date:日期
     * @return
     */
    public static Date getFirstDayOfMonth(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.set(Calendar.DAY_OF_MONTH, 1);
        return cal.getTime();
    }

    /**
     * 获取月份的第一天
     *
     * @param date:日期
     * @return
     */
    public static Date getFirstDateTimeOfMonth(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.set(Calendar.DAY_OF_MONTH, 1);

        cal.set(Calendar.HOUR_OF_DAY,0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        return cal.getTime();
    }

    /**
     * 获取月份的最后一天
     *
     * @param date:日期
     * @return
     */
    public static Date getLastDayOfMonth(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.MONTH, 1);
        cal.set(Calendar.DAY_OF_MONTH, 1);
        cal.add(Calendar.DAY_OF_MONTH, -1);
        return cal.getTime();
    }

    /**
     * 获取月份的最后一天
     *
     * @param date:日期
     * @return
     */
    public static Date getLastDateTimeOfMonth(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.MONTH, 1);
        cal.set(Calendar.DAY_OF_MONTH, 1);
        cal.add(Calendar.DAY_OF_MONTH, -1);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        return cal.getTime();
    }

    /**
     * 获取星期的第一天
     *
     * @param date:日期
     * @return
     */
    public static Date getFirstDayOfWeek(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.setFirstDayOfWeek(Calendar.MONDAY);
        cal.set(Calendar.DAY_OF_WEEK, cal.getFirstDayOfWeek()); // Monday
        return cal.getTime();
    }

    /**
     * 获取星期的最后一天
     *
     * @param date:日期
     * @return
     */
    public static Date getLastDayOfWeek(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.setFirstDayOfWeek(Calendar.MONDAY);
        cal.set(Calendar.DAY_OF_WEEK, cal.getFirstDayOfWeek() + 6);
        return cal.getTime();
    }

    /**
     * 获取上周第一天
     * @return
     */
    public static Date getFirstDayOfLastWeek(Date date) {
        Date firstDayOfWeek= getFirstDayOfWeek(date);
        return getAddDayNum(firstDayOfWeek,-7);
    }

    /**
     * 获取上周最后一天
     * @return
     */
    public static Date getLastDayOfLastWeek(Date date) {
        Date firstDayOfWeek= getFirstDayOfWeek(date);
        return  getAddDayNum(firstDayOfWeek,-1);
    }



    /**
     * 获取上月第一天
     * @return
     */
    public static Date getFirstDayOfLastMonth() {
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.MONTH, -1);
        calendar.set(Calendar.DAY_OF_MONTH,1);
        return calendar.getTime();
    }

    /**
     * 获取上月最后一天
     * @return
     */
    public static Date getLastDayOfLastMonth() {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.DAY_OF_MONTH, 0);
        return calendar.getTime();
    }

    public static String formatDateDuringDayBegin(Long totalSeconds) {
        Long days = totalSeconds / (60 * 60 * 24);
        Long hours = totalSeconds % (60 * 60 * 24) / (60 * 60);
        Long minutes = totalSeconds % (60 * 60 * 24) % (60 * 60) / 60;
        Long seconds = totalSeconds % (60 * 60 * 24) % (60 * 60) % 60 % 60;
        return days + "天" + hours + "时" + minutes + "分" + seconds + "秒";
    }

    public static String formatDateDurationHourBegin(Long totalSeconds) {
        Long hours = totalSeconds / (60 * 60);
        Long minutes = totalSeconds % (60 * 60) / 60;
        Long seconds = totalSeconds % (60 * 60) % 60 % 60;
        return (hours < 10 ? "0" + hours : hours) + ":" + (minutes < 10 ? "0" + minutes : minutes) + ":" + (seconds < 10 ? "0" + seconds : seconds);
    }

    public static String formatDateDurationMinBegin(Long totalSeconds) {
        Long minutes = totalSeconds / 60;
        Long seconds = totalSeconds % 60 % 60;
        return (minutes < 10 ? "0" + minutes : minutes) + ":" + (seconds < 10 ? "0" + seconds : seconds);
    }


    public static String formateDateString(String s,String pattern){
        if(McdStringUtil.isEmpty(s)){
            return "";
        }
        String s2 = null;
        SimpleDateFormat sdf = new SimpleDateFormat(pattern);
        try {
            Date date = sdf.parse(s);
            s2 = sdf.format(date);
        } catch (ParseException e) {

        }
        return s2;
    }

    public static Date getDateTimeByInMillis(long ms){
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(ms);
        return calendar.getTime();
    }



    public static String formateDatetimeStringForDate(Date date){
        SimpleDateFormat sdf = new SimpleDateFormat(YYYY_MM_DD_HH_MM_SS);
        String  s2 = sdf.format(date);
        return s2;
    }

    public static String formateDatetimeStringForDate(Date date,String pattern){
        SimpleDateFormat sdf = new SimpleDateFormat(pattern);
        String  s2 = sdf.format(date);
        return s2;
    }

    public static Date formatDateFromDatetime(Date date){
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(Calendar.HOUR_OF_DAY,0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        return calendar.getTime();
    }

    /**
     * 获取日期后几天的日期
     *
     * @param d
     * @param day
     * @return
     */
    public static Date getDateAfter(Date d, int day) {
        Calendar now = Calendar.getInstance();
        now.setTime(d);
        now.set(Calendar.DATE, now.get(Calendar.DATE) + day);
        return now.getTime();
    }

    /**
     * 月份加减后的日期
     * @param d 日期
     * @param m 几个月
     * @return
     */
    public static Date getDateByMonth(Date d,int m){
        Calendar now = Calendar.getInstance();
        now.setTime(d);
        now.add(Calendar.MONTH, m);
        return now.getTime();
    }

    public static Date getDateYearAndMonth(int year,int month){
        Calendar now = Calendar.getInstance();
        now.set(year,month-1,1);
        now.set(Calendar.HOUR_OF_DAY,0);
        now.set(Calendar.MINUTE, 0);
        now.set(Calendar.SECOND, 0);
        now.set(Calendar.MILLISECOND, 0);
        return  now.getTime();
    }

    /**
     * 加减日期
     * @param StringDate
     * @param numDays
     * @return
     */
    public static Date plusNumberDays(Date StringDate,int numDays){
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(StringDate);
        calendar.add(calendar.DATE, numDays);
        return calendar.getTime();
    }

    public static Date formatDateToNoTime(Date date){
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(Calendar.HOUR_OF_DAY,0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        return calendar.getTime();
    }

    public static double formatMsToHour(long ms){
        return DecimalUtil.round(ms/(3600*1000d),2);
    }

    /**
     * @Title: isInDate
     * @Description: 判断一个时间段（YYYY-MM-DD）是否在一个区间
     * @param @param date
     * @param @param strDateBegin
     * @param @param strDateEnd
     * @param @return    设定文件
     * @return boolean    返回类型
     * @throws
     */
    public static boolean isInDate(String strDate, String strDateBegin,String strDateEnd) {
        int  tempDate=Integer.parseInt(strDate.split("-")[0]+strDate.split("-")[1]+strDate.split("-")[2]);
        // 截取开始时间年月日 转成整型
        int  tempDateBegin=Integer.parseInt(strDateBegin.split("-")[0]+strDateBegin.split("-")[1]+strDateBegin.split("-")[2]);
        // 截取结束时间年月日   转成整型
        int  tempDateEnd=Integer.parseInt(strDateEnd.split("-")[0]+strDateEnd.split("-")[1]+strDateEnd.split("-")[2]);

        if ((tempDate >= tempDateBegin && tempDate <= tempDateEnd)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * 取本月共多少天
     * @param date
     * @return
     */
    public static int getDaysOfMonth(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        return calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
    }

    public static Date getDateNullInstance(){
        try {
            return  new SimpleDateFormat(YYYY_MM_DD_HH_MM_SS).parse(DATE_NULL_STR);
        } catch (ParseException e) {
        }
        return  null;
    }


}

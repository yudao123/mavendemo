package com.mcd.common.constant;

/**
 * @author heng.jia
 * @date 2018/6/20 下午9:19
 */
public enum CommonResponseType {
    ERROR, WARN
}

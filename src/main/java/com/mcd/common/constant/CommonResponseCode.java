package com.mcd.common.constant;

/**
 * @author heng.jia
 * @date 2018/6/20 下午9:18
 */
public enum CommonResponseCode {
    RC_SUCCESS("1", "success");
    private String responseCode;
    private String responseMessage;

    CommonResponseCode(String responseCode, String responseMessage) {
        this.responseCode = responseCode;
        this.responseMessage = responseMessage;
    }

    public String getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }

    public String getResponseMessage() {
        return responseMessage;
    }

    public void setResponseMessage(String responseMessage) {
        this.responseMessage = responseMessage;
    }
}
